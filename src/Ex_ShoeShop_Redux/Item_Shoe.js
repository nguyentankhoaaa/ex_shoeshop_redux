import React, { Component } from 'react'
import { connect } from 'react-redux';
import { addToCart } from './redux/action/ShoeAction';

 class Item_Shoe extends Component {
  render() {
    let {image,name,price} = this.props.shoe;
    return (
      <div className='col-3 p-2'>
    <div className="card">
  <img className="card-img-top" src={image} alt="Card image cap" />
  <div className="card-body">
    <h5 className="card-title">{name}</h5>
    <p className="card-text">{price}$</p>
    <a href="#" className="btn btn-success" onClick={()=>{
      this.props.handlePushToCart(this.props.shoe)
    }}>Add To Cart</a>
  </div>
</div>

      </div>
    )
  }
}

let mapDispatchToState = (dispatch)=>{
  return {
    handlePushToCart : (shoe)=>{
        dispatch(addToCart(shoe))
    } 
  }

} 

export default  connect(null,mapDispatchToState)(Item_Shoe)