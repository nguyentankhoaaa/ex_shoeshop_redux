import React, { Component } from 'react'
import { connect } from 'react-redux'
import Item_Shoe from './Item_Shoe'

 class List_Shoe extends Component {
  renderListShoe = ()=>{
    return this.props.listShoe.map((item)=>{
      return <Item_Shoe shoe = {item}/>
    })
  }
  render() {
    return (
      <div className='row'>
        {this.renderListShoe()}
      </div>
    )
  }
}

let mapStateToProp = (state) =>{
  return {
     listShoe:state.shoeReducer.listShoe
  }
}


export default connect(mapStateToProp)(List_Shoe)