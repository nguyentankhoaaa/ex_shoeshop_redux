import React, { Component } from 'react'
import Cart_ShoeShop from './Cart_ShoeShop'
import List_Shoe from './List_Shoe'

export default class Ex_ShoeShop_Redux extends Component {

    
  render() {
    return (
      <div>
        <div className="container">
            <h2 className='text-dark'>Shoes Shop</h2>
        <List_Shoe />
        <Cart_ShoeShop />
        <a href=""  data-toggle="modal" data-target="#exampleModalCenter">
        <i class="fa fa-shopping-cart"></i>
        </a>
        </div>
      </div>
    )
  }
}
