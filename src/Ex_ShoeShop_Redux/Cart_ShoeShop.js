import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ChangeQuanity, DeleteItem } from './redux/action/ShoeAction'


class Cart_ShoeShop extends Component {
    renderTbody = ()=>{
        return this.props.cart.map((item)=>{
          return <tr>
            <td>{item.id}</td>
            <td>{item.name}</td>
            <td>{item.price * item.soLuong}$</td>
            <td>
              <img src={item.image} style={{width:80}} alt="" />
            </td>
            <td>
              <button className='btn btn-danger' onClick={()=>{
               this.props.handleChangeQuanity(item.id,-1)
              }}>-</button>
              <strong>{item.soLuong}</strong>
              <button className='btn btn-success' onClick={()=>{
               this.props.handleChangeQuanity(item.id,1)
              }}>+</button>
              </td>
            <td><button className='btn btn-danger' onClick={()=>{
             this.props.handleDelete(item.id)            
            }}>Xóa</button></td>
          </tr>
        })
    }
  render() {
    return (
      <div>
{/* Modal */}
<div className="modal fade" id="exampleModalCenter" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div className="modal-dialog modal-dialog-centered" role="document">
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title" id="exampleModalLongTitle"><i class="fa fa-store"></i> ShoeShop</h5>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div className="modal-body">
        <table className="table">
            <thead>
                <th>ID</th>
                <th>Name</th>
                <th>Price</th>
                <th>Image</th>
                <th>Quanity</th>
                <th>Action</th>
            </thead>
            <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" className="btn btn-primary">Thanh toán</button>
      </div>
    </div>
  </div>
</div>

      </div>
    )
  }
}


let mapStateToProp = (state)=>{
return{
  cart:state.shoeReducer.cart,
}

}

let mapDispatchToProp = (dispatch)=>{
  return{
    handleDelete : (id)=>{
      dispatch(DeleteItem(id))
    },
    handleChangeQuanity : (id, num)=>{
       dispatch(ChangeQuanity(id,num))
    }
    
  }
}
export default  connect(mapStateToProp,mapDispatchToProp)(Cart_ShoeShop)