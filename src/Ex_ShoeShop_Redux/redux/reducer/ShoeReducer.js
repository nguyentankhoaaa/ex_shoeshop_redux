
import { dataShoe } from "../../Data_ShoeShop";
import { ADD_TO_CART, CHANGE_QUANITY, DELETE_ITEM } from "../constant/ShoeConstant";

let initialValue = {
    listShoe:dataShoe,
    cart:[],
 
}
export const shoeReducer =(state= initialValue , action)=>{
switch(action.type){
case ADD_TO_CART:{
  let cloneCart = [...state.cart];
  let index = state.cart.findIndex((item)=>{
    return item.id == action.payload.id;
  })
  if (index == -1){
    let newShoe = {...action.payload,soLuong:1};
    cloneCart.push(newShoe);
  }else{
    cloneCart[index].soLuong++;
  }
  return {...state,cart:cloneCart}

}
case DELETE_ITEM:{
    let newCart = state.cart.filter((item)=>{
        return item.id !=  action.payload;
    })
    return{...state,cart:newCart}

}
case CHANGE_QUANITY:{
  let cloneCart = [...state.cart];
  let index = state.cart.findIndex((item)=>{
    return item.id == action.payload_1;
  })
  if(index != -1){
    cloneCart[index].soLuong += action.payload_2;
  }
  if(cloneCart[index].soLuong  == 0){
    cloneCart.splice(index,1)
  }
  return{...state,cart:cloneCart}
}

    default:
        return state;
}
}