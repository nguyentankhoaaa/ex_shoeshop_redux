import { ADD_TO_CART, CHANGE_QUANITY, DELETE_ITEM } from "../constant/ShoeConstant"

export const addToCart = (shoe)=>{
    return{
        type:ADD_TO_CART,
        payload:shoe,
    }
}


export const DeleteItem = (id)=>{
    return {
        type:DELETE_ITEM,
        payload:id,
    }
}
export const ChangeQuanity = (id,num)=>{
return {
    type:CHANGE_QUANITY,
    payload_1:id,
    payload_2:num,
}
}   